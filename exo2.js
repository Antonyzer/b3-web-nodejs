function add(number) {
    const numbers = number.split(',');
    let result = 0;

    for (let i = 0; i < numbers.length; i++) {
        result += parseFloat(numbers[i]);
    }
    return result;
}

console.log(add("0"));
console.log(add("1"));
console.log(add("1.1,2.2"));
